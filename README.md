# nv3dfi-boost

This git repository was created in order to simplify compilation and usage
of Boost.Beast and Boost.Filesystem libraries.

The top level CMakeList.txt compiles the boost.System and boost.Filesystem
source code into a single library and expose all headers that are required for
the compilation of an asynchronous HTTP server as well as filesystem tools.

## shell script that was used to populate all git submodules required for beast
## and filesystem:

```shell
# Boost.Beast dependencies
git submodule add https://github.com/boostorg/system.git ./libs/system
git submodule add https://github.com/boostorg/beast.git ./libs/beast
git submodule add https://github.com/boostorg/asio.git ./libs/asio
git submodule add https://github.com/boostorg/config.git ./libs/config
git submodule add https://github.com/boostorg/optional.git ./libs/optional
git submodule add https://github.com/boostorg/core.git ./libs/core
git submodule add https://github.com/boostorg/bind ./libs/bind
git submodule add https://github.com/boostorg/static_assert.git ./libs/
git submodule add https://github.com/boostorg/static_assert.git ./libs/static_assert
git submodule add https://github.com/boostorg/utility.git ./libs/utility
git submodule add https://github.com/boostorg/throw_exception.git ./libs/throw_exception
git submodule add https://github.com/boostorg/assert.git ./libs/assert
git submodule add https://github.com/boostorg/predef.git ./libs/predef
git submodule add https://github.com/boostorg/type_traits.git ./libs/type_traits
git submodule add https://github.com/boostorg/intrusive.git ./libs/intrusive
git submodule add https://github.com/boostorg/move.git ./libs/move
git submodule add https://github.com/boostorg/smart_ptr.git ./libs/smart_ptr
git submodule add https://github.com/boostorg/date_time.git ./libs/date_time
git submodule add https://github.com/boostorg/mpl.git ./libs/mpl
git submodule add https://github.com/boostorg/preprocessor.git ./libs/preprocessor
git submodule add https://github.com/boostorg/numeric_conversion.git ./libs/numeric/conversion
git submodule add https://github.com/boostorg/functional.git ./libs/functional
git submodule add https://github.com/boostorg/endian.git ./libs/endian

# Boost.Filesystem dependencies
git submodule add https://github.com/boostorg/filesystem ./libs/filesystem
git submodule add https://github.com/boostorg/iterator ./libs/iterator
git submodule add https://github.com/boostorg/detail ./libs/detail
git submodule add https://github.com/boostorg/io ./libs/io
git submodule add https://github.com/boostorg/functional ./libs/functional
git submodule add https://github.com/boostorg/container_hash ./libs/container_hash
git submodule add https://github.com/boostorg/range ./libs/range
```
